/* smirnov_transform.h

    \brief 
    The output of any random system can be plotted as a histogram where
    the 'x' axis represents each discrete outcome, and 'y' is the number of times
    that outcome occurs.
    https://en.wikipedia.org/wiki/Histogram
    
    In a large enough sample space, the normalised probability of each outcome 
    will stabilise and allow us to predict the probability density function (PDF)
    for the system.
    https://en.wikipedia.org/wiki/Probability_density_function
    
    This class is designed to provide the inverse function of this technique by 
    allowing the user to pass in a set of data representing the historgram, or a
    discretely sampled PDF, and reproducing that distribution once queried.
    
    The technique itself is refered to as Inverse Transform Sampling and was 
    researched by Nikolai Smirnov.
    https://en.wikipedia.org/wiki/Inverse_transform_sampling
    https://en.wikipedia.org/wiki/Nikolai_Smirnov_(mathematician)
    
    
    Hence, the Smirnov Transform
    
    
    Usage:
    The transform's T_x, can be anything you want returned as your 
    random value type. It should be copy assignable / constructable.
      An int, float, struct, pointer, it should not matter.
    The transform's T_y, must be an arithmetic type that can be summised to
      calculate the normal distribution. The class will not compile if you
      attempt to do otherwise.
    
    Initialization:
    Method A: Pair Initialisation
    Populate an std::vector with a collection of smirnov_pair s that represent 
    your histogram. Pass this vector to the constructor, 
    then the transform will be fully initialized.
    
    Method B: Vector Initialisation
    You can populate a vector with just Y values, that is 
    std::vector<float or int or etc>. The T_x flag must then be numeric non-bool.
      When sampled in this case:
      T_x float will return values as a ratio 0 and 1 based on the vector's size
        representing how far along the axis, the column for T_y, ends.
        ie. this means 0 is impossible - the lowest value is 1.0f / vector.size()
      T_x integer type will return x values starting at 0, through to 
        vector.size() - 1. Good as an indexer if testing & recreating the
        histogram.
    
    Sampling:
    Call sample() to receive a random T_x based on your histogram distribution.
    Call it thousands of times and track the output to see the histogram rebuilt.
    
    Call sample_with_ratio() to receive the a smirnov_pair with the normalized
    probability of that outcome stored inside.
    
    
    Closing Notes:
    Expected usage is for loot tables and procedural generation, where a curve,
    discretely sampled, is preferable to individual object / odds tweeking.
    
    The sample() is a binary search of the valid data set O(log n) 
      average performance so do not be afraid of high fidelity data sets
    
    A visual histogram tool with spline editing that drops its samples into a 
    simple format would be a game designers dream with this transform
    
    Adam Clarke - 2020 02 19
                - 2021 09 21 - Updated binary search and completed comments
*/
#pragma once
#ifndef SMIRNOV_TRANSFORM_H
#define SMIRNOV_TRANSFORM_H

#include <iterator>
#include <memory>
#include <vector>

/* Random number generator - Use your own here if desired */
#include "rng.h"

/* Forward declare transform for single friend function before definition*/
template< typename T_x, typename T_y,
	typename std::enable_if < std::is_arithmetic_v<T_y>>::type* = nullptr >
class smirnov_transform;

/* \brief Smirnov_pair provides a type-valid key value pair for a 
    probability historgram inorder to perform inverse transform sampling
   \tparam T_x any type, an outcome from the 'x' axis of the distribution
   \tparam T_y any numeric type, representing the frequency of the outcome
*/
template< typename T_x, typename T_y,
	typename std::enable_if < std::is_arithmetic_v<T_y>>::type * = nullptr >
class smirnov_pair
{
private:
	/* \brief The value coordinate along the x axis 
       \note This does not have to be unique 
    */
	T_x discrete_sample;

    /* \brief The value coordinate along the y axis
	   \note This must be numeric
	*/
	T_y sample_frequency;

	/* \brief The normalised frequency of this sample
	   \note Between 0 and 1
	*/
	double sample_membership;

public:
	/* \brief Construction of a single pair from values
       \param A single discrete value from the x axis of the histogram
       \param The frequency value for this sample
    */
    constexpr smirnov_pair(const T_x& a_discrete_sample,
                           const T_y& a_sample_frequency) noexcept :
        discrete_sample(a_discrete_sample),
        sample_frequency(a_sample_frequency),
        sample_membership(0) 
	{}
	
	/* \brief Must be constucted by type with value 
	*/
	smirnov_pair() = delete;

    /** Rule of 5 default implementation but noexcept for vector speed
        The trivial types require no special treatment beyond.
        https://www.youtube.com/watch?v=AG_63_edgUg
    **/

    /* \brief Default destructor
	*/
    ~smirnov_pair() noexcept = default;

    /* \brief Default copy constructor
    */
    smirnov_pair(const smirnov_pair& a_other) noexcept = default;

    /* \brief Default move constructor
    */
    smirnov_pair(smirnov_pair&& a_other) noexcept = default;

    /* \brief Default copy assignment operator
    */
    smirnov_pair& operator=(const smirnov_pair & a_other) noexcept = default;

    /* \brief Default move assignment operator
    */
    smirnov_pair& operator=(smirnov_pair && a_other) noexcept = default;

	/* \brief discrete_sample by copy
       \return Copy of discrete_sample 
	*/
    [[nodiscard]]
	constexpr T_x get_x() const noexcept
	{
		return discrete_sample;
	}

    /* \brief sample_frequency by copy
       \return Copy of sample_frequency
    */
    [[nodiscard]]
    constexpr T_y get_y() const noexcept
	{
		return sample_frequency;
	}

    /* \brief sample_membership by copy, a value between 0 and 1
       \return Copy of sample_membership value
    */
    [[nodiscard]]
	double get_y_normalized() const noexcept
	{
		return sample_membership;
	}

private:
    /* \brief Friend class declaration for setting the normalized member-
       ship value
    */
    friend class smirnov_transform<T_x, T_y>;

    /* \brief Set the normalized value of this pair from the transform
       \param The calculated normalised membership value
    */
    constexpr void set_y_normalized(const double a_normalized) noexcept
    {
        sample_membership = a_normalized;
    }
};

/* \brief smirnof_transform provides a type-valid key value pair vector 
    processed and arranged as a histogram to perform inverse transform sampling
   \tparam T_x any type, an outcome from the 'x' axis of the distribution
   \tparam T_y any numeric type, representing the frequency of the outcome
*/
template< typename T_x, typename T_y,
	typename std::enable_if < std::is_arithmetic_v<T_y>>::type * >
class smirnov_transform
{
private:
    /* \brief Encapsulated histogram data pairs
    */
	std::vector<smirnov_pair<T_x, T_y>> valid_data;

    /* \brief Ordered set of increasing sample membership values to act as the
       search space when sampling
    */
	std::unique_ptr<double[]> summised_membership_values = nullptr;
    
    /* \brief The member rng device for this transform
       \note Replace with a lighter weight one if desired
    */
    random_number_generator rng;

public:
    /* \brief
    */
    [[nodiscard]] constexpr
        smirnov_transform(
            const std::vector<smirnov_pair<T_x, T_y>>& a_smirnov_pair_vector)
        noexcept
    {
        initialize(a_smirnov_pair_vector);
    }

    /* \brief Constructor from just a set of T_y values. T_x type indicates 
       whether sample returns a 0 to (length - 1) int or a float between
       (1 / length) and 1 for the normalised distance along the X axis
       \tparam This is only a valid constructor for T_x arithmetic, T_y is 
       arithmetic by class template regardless.
       \param The vector of T_y data for histogram values.
    */
    template
    < typename std::enable_if< std::is_arithmetic_v<T_x>>::type * = nullptr >
    [[nodiscard]] constexpr 
        smirnov_transform(const std::vector<T_y> &a_smirnov_pair_vector)
        noexcept
	{
        // Build a smirnov_pair vector and pass it to the regular constructor
        std::vector<smirnov_pair<T_x, T_y>> smirnov_pair_vector;
        for (size_t index = 0; index < a_smirnov_pair_vector.size(); ++index)
        {
            // if floating point, Take the right value of the T_x range
            if constexpr (std::is_floating_point<T_x>::value)
            {
                smirnov_pair_vector.push_back(
                    smirnov_pair<T_x, T_y>(
                        float(index + 1) / a_smirnov_pair_vector.size(),
                        a_smirnov_pair_vector[index]));
            }
            else // Must be whole type, take the T_x as index
            {
                smirnov_pair_vector.push_back(
                    smirnov_pair<T_x, T_y>(
                        T_x(index),
                        a_smirnov_pair_vector[index]));
            }
        }

        // Construct
        initialize(smirnov_pair_vector);
	}

    /* \brief Sample a random X value from the transform
       \return T_x value at random based on inital membership
    */
	[[nodiscard]] constexpr const T_x sample() noexcept
	{
        return sample_pair().get_x();
	}

    /* \brief Sample a random smirnov_pair from the transform
       \return smirnov_pair<T_x, T_y> Both the T_x object and its membership
    */
    [[nodiscard]] 
    constexpr smirnov_pair<T_x, T_y> sample_pair() noexcept
	{
        // Our normal range sample point
		double ratio = rng.get<double>();

        // Indicies for binary search to locate smirnov_pair that contains the
        // random membership ratio from above.
		size_t low = 0;
		size_t high = valid_data.size() - 1;
		size_t mid = 0;
		size_t found_index = 0;

		bool found = false;

        // While not found and not an error...
		while (low <= high && !found)
		{
            // Split the seach space
			mid = (low + high) / 2;

            // Low-half check
			if (ratio < summised_membership_values[mid])
			{
                // It was the first element all along
				if (mid == 0)
				{
					found_index = mid;
					found = true;
				}
                else // Bring down the high index
                {
                    high = mid - 1;
                }
			}
			else if (ratio >= summised_membership_values[mid])
			{
                // (Extreme) Rare exact hit
				if (ratio == summised_membership_values[mid])
				{
					found_index = mid;
					found = true;
				}
                // Move up the low index
				low = mid + 1;
                // and check.
				if (ratio <= summised_membership_values[low])
				{
					found_index = low;
					found = true;
				}
			}

		}
		if (!found)
		{
			found_index = mid;
		}
		return { valid_data[found_index] };
	}

private:
    /*  \brief Copys the valid & useful data from the input set
        \param Vector of pairs to be processed
    */
    constexpr void initialize( 
        const std::vector<smirnov_pair<T_x, T_y>>& a_smirnov_pair_vector )
        noexcept
    {
        // Copy our vector of pairs
        std::copy(a_smirnov_pair_vector.begin(),
            a_smirnov_pair_vector.end(),
            std::back_inserter(valid_data));

        // remove any T_x with a membership of 0 or less as
        // they will never sample
        for (size_t index = 0; index < valid_data.size(); /*iterate on !erase*/)
        {
            if (valid_data[index].get_y() <= 0)
                valid_data.erase(valid_data.begin() + index);
            else
                ++index;
        }

        // Process the data and calculate each pair's membership
        calculate_membership();
    }

    /*  \brief Calculates the total weight of the set, before then calculating
        and storing the normalised weight of each pair.
    */
    constexpr void calculate_membership() noexcept
    {
        // The total sum of all T_y in order to calculate normalized membership
        double summation_y =
            subdivide_and_sum(valid_data.data(), valid_data.size());

        // Calculate the total membership from zero to any index n
        auto normalized_y_axis_values =
            std::make_unique<double[]>(valid_data.size());

        // Iterate over all data...
        for (size_t index = 0; index < valid_data.size(); ++index)
        {
            // ...calculating normalized membership
            normalized_y_axis_values[index] =
                double(valid_data[index].get_y() / summation_y);
            // Store in each
            valid_data[index].set_y_normalized(normalized_y_axis_values[index]);
        }

        // Initialize the container for our sample search space
        summised_membership_values =
            std::make_unique<double[]>(valid_data.size());

        // Store the accumulated membership to each index
        for (size_t index = 0; index < valid_data.size(); ++index)
        {
            summised_membership_values[index] =
                subdivide_and_sum(&normalized_y_axis_values[0], index + 1);
        }
    }

    /* \brief Recursive sub-division addition to reduce errors in floating point
        accumulation (Adding a tea spoon of water to a swimming pool)
	https://stackoverflow.com/questions/26344144/divide-and-conquer-algorithm-for-sum-of-integer-array
    */
    template< typename T >
    [[nodiscard]] constexpr 
    double subdivide_and_sum(T* data_collection, size_t size) const noexcept
    {
        //base case
        if (size == 0)
        {
            return 0;
        }
        else if (size == 1)
        {
            //if constexpr (std::is_same_v<data, double>>)
            if constexpr (std::is_same<T, double>::value)
                return data_collection[0];
            if constexpr (std::is_same<T, smirnov_pair<T_x, T_y>>::value)
                return double(data_collection[0].get_y());
        }

        //divide and conquer
        size_t mid = size / 2;
        size_t rsize = size - mid;
        double lsum = subdivide_and_sum(data_collection, mid);
        double rsum = subdivide_and_sum(data_collection + mid, rsize);
        return lsum + rsum;
    }
};

#endif