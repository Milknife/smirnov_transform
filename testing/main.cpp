#include "..\smirnov_transform\smirnov_transform.h"
#include <string>
#include <iostream>

int main()
{
    /** Y VALUE-ONLY INITIALISATION OF A SIN()**/
    std::vector<double> vector_of_sin;
    for (size_t i = 0; i < 10; i++)
    {
        // 2 * sin(x/pi) + 2
        vector_of_sin.push_back(2 * sin(i/3.14159) + 2ll);
    }
    // Use these as the 'y' values of a Probability density function
    smirnov_transform<int, double> sin_transform(vector_of_sin);

    // For logging of results
    int output_of_sin[10]{ 0 };

    // 1024 = 2^10 * 1000
    // Sample our PDF over a million times
    for (int index = 0; index < 1024000; ++index)
    {
        // Result will be the 'x', the power of two that we rolled.
        int result = sin_transform.sample();
        // Count the output
        output_of_sin[result]++;
    }
    // Display the final count of the samples
    std::cout << "Sin()\n";
    for (int index = 0; index < 10; ++index)
    {
        std::cout << vector_of_sin[index] <<
            "\t:\t" << output_of_sin[index] << "\n";
    }
    
    std::cin.get();

    /** Y VALUE-ONLY INITIALISATION **/
    // Push the first 10 powers of 2 into a vector
	std::vector<size_t> vector_of_powers;
    for (size_t i = 0; i < 10; i++)
    {
        // 1 << i = 2^i
        vector_of_powers.push_back(1ll << i);
    }
    // Use these as the 'y' values of a Probability density function
	smirnov_transform<int, size_t> powers_transform(vector_of_powers);

    // For logging of results
    int output_ints[10]{ 0 };

    // 1024 = 2^10 * 1000
    // Sample our PDF over a million times
    for (int index = 0; index < 1024000; ++index)
    {
        // Result will be the 'x', the power of two that we rolled.
        int result = powers_transform.sample();
        // Count the output
        output_ints[result]++;
    }

    // Display the final count of the samples
    std::cout << "Value only sample\n";
    for (int index = 0; index < 10; ++index)
    {
        std::cout << vector_of_powers[index] <<
            "\t:\t" << output_ints[index] << "\n";
    }

    /** PAIR BASED INITIALISATION **/
    //Create our pairs as the first 10 powers of 2
    std::vector<smirnov_pair<int, float>> vector_of_pairs = 
    {
      //{ Object data / reference / key , set weight of object },
        {0, 1.0f},
        {1, 2.0f},
        {2, 4.0f},
        {3, 8.0f},
        {4, 16.0f},
        {5, 32.0f},
        {6, 64.0f},
        {7, 128.0f},
        {8, 256.0f},
        {9, 512.0f},
    };

    // Use these as the pairs to construct a Probability density function
	smirnov_transform<int, float> pairs_transform(vector_of_pairs);

    // For logging of results
	int output_pairs[10]{ 0 };

    // 1024 = 2^10 * 1000
    // Sample our PDF over a million times
	for (int index = 0; index < 1024000; ++index)
	{
		int result = pairs_transform.sample_pair().get_x();
        output_pairs[result]++;
	}

    // Display the final count of the samples
    std::cout << "\nPair Sample\n";
	for (int index = 0; index < 10; ++index)
	{
		std::cout << vector_of_pairs[index].get_y() <<
            "\t:\t" << output_pairs[index] << "\n";
	}

    // Any key to continue...
    std::cin.get();
}